import XCTest
@testable import Cards

class YanivEvaluationTests: XCTestCase {
    
    let discardStrategies: [DiscardStrategy.Type] = [
                DiscardLowest.self,
                DiscardHighest.self,
                DiscardMin3Sequence.self,
                DiscardMin4Sequence.self,
                DiscardMin5Sequence.self,
                DiscardLowestMin2Matches.self,
                DiscardLowestMin3Matches.self,
                DiscardLowestMin4Matches.self,
                DiscardHighestMin2Matches.self,
                DiscardHighestMin3Matches.self,
                DiscardHighestMin4Matches.self,
    ]
    
    let drawStrategies: [DrawStrategy.Type] = [
                DrawJoker.self,
                DrawHighestCardThatMatches.self,
                DrawLowestCardThatSequences.self,
                DrawLowestCardThatSequences.self,
                DrawHighestCardThatSequences.self
    ]
    
    func generateRandomPlayers(amount: Int) -> [YanivPlayer] {
        var players = [YanivPlayer]()
        
        for i in 0..<amount {
            // Choose some random discard strategies
            let discardR = Int(arc4random_uniform(UInt32(discardStrategies.count)))
            var discards: [DiscardStrategy.Type] = []
            let shuffledDiscards = discardStrategies.shuffled()
            for j in 0..<discardR {
                discards.append(shuffledDiscards[j])
            }
            
            // Choose some random draw strategies
            let drawR = Int(arc4random_uniform(UInt32(drawStrategies.count)))
            var draws: [DrawStrategy.Type] = []
            let shuffledDraws = drawStrategies.shuffled()
            for j in 0..<drawR {
                draws.append(shuffledDraws[j])
            }
            
            let strategy = Strategy(discardPreferences: discards, drawPreferences: draws)
            let newPlayer = createPlayer(named: "🤖 Player \(i+1)", strategy: strategy)
            players.append(newPlayer)
        }
        
        return players
    }
    
    func createPlayer(named: String, strategy: Strategy) -> YanivPlayer {
        let player = YanivPlayer(name: named)
        player.strategy = strategy
        return player
    }
    
    func compete(yanivPlayers: [YanivPlayer]) -> String? {
        let yaniv = Yaniv(players: yanivPlayers)
        yaniv.debugTracking = false
        yaniv.evaluate()
        
        // After the game is over...
        let scores = yanivPlayers.filter({$0.score < 200})
        if let winner = scores.first, scores.count == 1 {
            return winner.name
        }
        
        return nil
    }
    
    func evaluate(_ playerTemplates: [YanivPlayer]) -> YanivPlayer {
        var gamesWon = [String: Int]()
        for player in playerTemplates {
            gamesWon[player.name] = 0
        }
        
        var numberOfAbandonedGames = 0
        
        50.times {
            var yanivPlayers = [YanivPlayer]()
            for i in 0..<playerTemplates.count {
                let p = createPlayer(named: playerTemplates[i].name, strategy: playerTemplates[i].strategy)
                yanivPlayers.append(p)
            }
            
            if let winner = compete(yanivPlayers: yanivPlayers) {
                gamesWon[winner]! += 1
            } else {
                numberOfAbandonedGames += 1
            }
        }
        
        let sortedCohort = playerTemplates.sorted(by: { (playerA, playerB) -> Bool in
            gamesWon[playerA.name]! > gamesWon[playerB.name]!
        })
        print("\n\n👾👾👾 RESULTS 👾👾👾\n\n")
        for player in sortedCohort {
            print("\(player.name) won \(gamesWon[player.name]!) games")
            print("\(player.strategy)\n")
        }
        print("\n\(numberOfAbandonedGames) games abandoned.")
        print("\n\n👾👾👾👾👾👾👾👾👾👾\n\n")
        
        return sortedCohort.first!
    }
    
    func mutated(strategy: Strategy) -> Strategy {
        var newStrategy = strategy
        
        let r = arc4random_uniform(8)
        
        switch r {
        case 0:
            // Shuffle the discard preferences
            newStrategy = Strategy(discardPreferences: newStrategy.discardPreferences.shuffled(),
                                   drawPreferences: newStrategy.drawPreferences)
        case 1:
            // Shuffle the draw preferences
            newStrategy = Strategy(discardPreferences: newStrategy.discardPreferences,
                                   drawPreferences: newStrategy.drawPreferences.shuffled())
        case 2:
            // Shuffle all preferences
            newStrategy = Strategy(discardPreferences: newStrategy.discardPreferences.shuffled(),
                                   drawPreferences: newStrategy.drawPreferences.shuffled())
        case 3:
            // Remove the first item from discard prefs
            var newDiscardPreferences = newStrategy.discardPreferences
            if newDiscardPreferences.count > 0 {
                newDiscardPreferences.remove(at: 0)
            }
            
            newStrategy = Strategy(discardPreferences: newDiscardPreferences,
                                   drawPreferences: newStrategy.drawPreferences)
        case 4:
            // Remove the first item from draw prefs
            var newDrawPreferences = newStrategy.drawPreferences
            if newDrawPreferences.count > 0 {
                newDrawPreferences.remove(at: 0)
            }
            
            newStrategy = Strategy(discardPreferences: newStrategy.discardPreferences,
                                   drawPreferences: newDrawPreferences)
        case 5:
            // Add item to draw preferences
            var newDrawPreferences = newStrategy.drawPreferences
            
            if drawStrategies.count > 0 {
                let drawR = Int(arc4random_uniform(UInt32(drawStrategies.count)))
                let newDraw = drawStrategies[drawR]
                if !newDrawPreferences.contains(where: { strategy -> Bool in
                    strategy.description == newDraw.description
                }) {
                    newDrawPreferences.insert(newDraw, at: 0)
                }
            }
            
            newStrategy = Strategy(discardPreferences: newStrategy.discardPreferences,
                                   drawPreferences: newDrawPreferences)
        case 6:
            // Add item to discard preferences
            var newDiscardPreferences = newStrategy.discardPreferences
            
            if discardStrategies.count > 0 {
                let discardR = Int(arc4random_uniform(UInt32(discardStrategies.count)))
                let newDiscard = discardStrategies[discardR]
                
                if !newDiscardPreferences.contains(where: { strategy -> Bool in
                    strategy.description == newDiscard.description
                }) {
                    newDiscardPreferences.insert(newDiscard, at: 0)
                }
            }
            
            newStrategy = Strategy(discardPreferences: newDiscardPreferences,
                                   drawPreferences: newStrategy.drawPreferences)
        default:
            break
        }
        
        return newStrategy
    }
    
    func generate(players amount: Int, with strategy: Strategy?) -> [YanivPlayer] {
        guard let strategy = strategy else {
            return generateRandomPlayers(amount: amount)
        }
        
        var players = [YanivPlayer]()
        for i in 0..<amount-2 {
            let p = createPlayer(named: "👿 Player \(i)", strategy: mutated(strategy: strategy))
            players.append(p)
        }
        
        let randomPlayer = generate(players: 1, with: nil).first!
        randomPlayer.name = "💩 Player \(amount-2)"
        players.append(randomPlayer)
        players.append(createPlayer(named: "👺 Player \(amount-1)", strategy: strategy))

        return players
    }
    
    func testStrategiesGeneticAlgorithm() {
        var bestOverall: YanivPlayer?
        
        5.times {
            let cohort = generate(players: 4, with: bestOverall?.strategy)
            bestOverall = evaluate(cohort)
            
            print(bestOverall!.strategy)
        }
    }
    
}
