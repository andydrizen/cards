import Foundation
public protocol CardCharacteristic: CustomStringConvertible, Hashable {}

public enum Suit: CardCharacteristic {
    case club
    case diamond
    case heart
    case spade
    case joker
    
    public var description: String {
        switch self {
        case .club:
            return "♣️"
        case .diamond:
            return "♦️"
        case .heart:
            return "♥️"
        case .spade:
            return "♠️"
        case .joker:
            return "🤡"
        }
    }
}

public enum Face: CardCharacteristic {
    case ace
    case two
    case three
    case four
    case five
    case six
    case seven
    case eight
    case nine
    case ten
    case jack
    case queen
    case king
    case joker
    
    public var description: String {
        switch self {
        case .ace:
            return "A"
        case .two:
            return "2"
        case .three:
            return "3"
        case .four:
            return "4"
        case .five:
            return "5"
        case .six:
            return "6"
        case .seven:
            return "7"
        case .eight:
            return "8"
        case .nine:
            return "9"
        case .ten:
            return "10"
        case .jack:
            return "J"
        case .queen:
            return "Q"
        case .king:
            return "K"
        case .joker:
            return "🤡"
        }
    }
}

public struct Card: CustomStringConvertible, Equatable, Hashable {
    public let face: Face
    public let suit: Suit
    public var hashValue: Int {
        return description.hashValue
    }
    
    public var description: String {
        return "\(face)\(suit)"
    }
    
    public init(face: Face, suit: Suit) {
        self.face = face
        self.suit = suit
    }
    
    public static func ==(lhs: Card, rhs: Card) -> Bool {
        return lhs.face == rhs.face && lhs.suit == rhs.suit
    }
}
