import UIKit

public class CardView: UIView {
    
    
    public var card: Card? {
        didSet {
            update()
        }
    }
    public var isFaceUp: Bool = true {
        didSet {
            update()
        }
    }

    private var suitLabel = UILabel()
    private var faceLabel = UILabel()
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit() {
        addSubview(suitLabel)
        addSubview(faceLabel)
        layer.borderWidth = 1
        layer.cornerRadius = 12
        clipsToBounds = true
        
        faceLabel.textAlignment = .center
        faceLabel.font = UIFont.boldSystemFont(ofSize: 40)
        
        suitLabel.textAlignment = .center
        suitLabel.font = UIFont.boldSystemFont(ofSize: 40)
    }
    
    override public func layoutSubviews() {
        super.layoutSubviews()
        faceLabel.frame = CGRect(x: 0, y: 0, width: frame.width, height: frame.height/2)
        suitLabel.frame = CGRect(x: 0, y: frame.height/2, width: frame.width, height: frame.height/2)
    }
    
    func update() {
        if isFaceUp {
            backgroundColor = .white
            
            guard let card = card else { return }
            suitLabel.text = card.suit.description
            faceLabel.text = card.face.description
            suitLabel.isHidden = false
            faceLabel.isHidden = false
            layer.borderWidth = 1
            layer.borderColor = UIColor.lightGray.cgColor
        } else {
            backgroundColor = UIColor(red:0.19, green:0.46, blue:0.72, alpha:1.00)
            suitLabel.isHidden = true
            faceLabel.isHidden = true
            layer.borderWidth = 6
            layer.borderColor = UIColor.white.cgColor
        }
    }
    
}
