import Foundation

public class YanivPlayer: CustomStringConvertible {
    public private(set) var score: Int
    public var hand: Deck
    public var strategy: Strategy
    public var name: String
    
    public init(name: String) {
        self.name = name
        score = 0
        hand = Deck()

        strategy = Strategy(
            discardPreferences: [
                DiscardHighest.self
            ],
            drawPreferences: [])
    }
    
    public var description: String {
        return "\(name)> Score: \(score). Current hand: \(hand). Strategy: \(strategy)"
    }
    
    public func increaseScore(by: Int) {
        score += by
    }
    
    public func move(availableDiscardedCards: [Card], playerIndex: Int) -> YanivTurn {
        return strategy.move(currentHand: hand, availableDiscardedCards: availableDiscardedCards, playerIndex: playerIndex)
    }
}
