import Foundation

struct DiscardLowest: DiscardStrategy {
    static var description: String = "DiscardLowest"
    
    static func discard(from currentHand: Deck) -> DiscardStrategyResult {
        let discard = currentHand.sorted(by: Deck.yanivSort).first!
        return DiscardStrategyResult(discardCards: [discard])
    }
}
