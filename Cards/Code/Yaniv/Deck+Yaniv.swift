import Foundation

extension Array where Element == Card {
    
    var yanivFaceValue: Int {
        var value = 0
        for card in self {
            switch card.face {
            case .joker:
                value += 0
            case .ace:
                value += 1
            case .two:
                value += 2
            case .three:
                value += 3
            case .four:
                value += 4
            case .five:
                value += 5
            case .six:
                value += 6
            case .seven:
                value += 7
            case .eight:
                value += 8
            case .nine:
                value += 9
            case .ten:
                value += 10
            case .jack:
                value += 10
            case .queen:
                value += 10
            case .king:
                value += 10
            }
        }
        return value
    }
    
    var yanivRawValue: Int {
        var value = 0
        for card in self {
            switch card.face {
            case .joker:
                value += 0
            case .ace:
                value += 1
            case .two:
                value += 2
            case .three:
                value += 3
            case .four:
                value += 4
            case .five:
                value += 5
            case .six:
                value += 6
            case .seven:
                value += 7
            case .eight:
                value += 8
            case .nine:
                value += 9
            case .ten:
                value += 10
            case .jack:
                value += 11
            case .queen:
                value += 12
            case .king:
                value += 13
            }
        }
        return value
    }
    
    var yanivSuitValue: Int {
        var value = 0
        for card in self {
            switch card.suit {
            case .joker:
                value += 0
            case .club:
                value += 1
            case .diamond:
                value += 2
            case .heart:
                value += 3
            case .spade:
                value += 4
            }
        }
        return value
    }
    
    public static func yanivSort(a: Card, b: Card) -> Bool {
        if [a].yanivRawValue == [b].yanivRawValue {
            return [a].yanivSuitValue < [b].yanivSuitValue
        }
        
        return [a].yanivRawValue < [b].yanivRawValue
    }
    
    // returns the largest gap and offset of the start of the gap
    public static func numberOfGapsIn(sequence: [Card]) -> Int {
        if sequence.count == 0 {
            return 0
        }
        
        var numberOfGaps = 0
        
        var offset = [sequence.first!].yanivRawValue
        
        for card in sequence {
            let expectedValue = offset
            let actualValue = [card].yanivRawValue
            let gap = actualValue - expectedValue
            numberOfGaps += gap
            offset += 1 + gap
        }
        
        return numberOfGaps
    }
    
    static func largestSequence(inCards: [Card], withGapLessThan gapTheshold: Int) -> [Card] {
        var largestSequence: [Card] = []
        var newSequence = [Card]()
        
        let cardsGroups = inCards
            .filter({$0.suit != .joker})
            .groupBy(characteristic: Suit.self, sortedBy: Deck.yanivSort)
            .filter({$0.count > 1})
        
        for cards in cardsGroups {
            for i in 0..<cardsGroups.count {
                for length in 1..<cards.count-i+1 {
                    let subsequence = Array(cards[i..<i+length])
                    let numberOfGaps = numberOfGapsIn(sequence: subsequence)
                    if numberOfGaps <= gapTheshold && subsequence.count >= largestSequence.count {
                        largestSequence = subsequence
                    } else {
                        break
                    }
                }
            }
            
            if largestSequence.count > 1 {
                newSequence.append(largestSequence.first!)
                var cardValues = largestSequence.map({[$0].yanivRawValue})
                
                for i in 1..<cardValues.count {
                    let numberOfJokersToAdd = cardValues[i] - cardValues[i-1] - 1
                    numberOfJokersToAdd.times {
                        newSequence.append(Card(face: .joker, suit: .joker))
                    }
                    newSequence.append(largestSequence[i])
                }
            }
        }
        
        return newSequence
    }
    
}
