import Foundation

struct DiscardMin3Sequence: DiscardStrategy {
    static var description: String = "DiscardMin3Sequence"
    
    static func discard(from currentHand: Deck) -> DiscardStrategyResult {
        let numberOfJokersInHand = currentHand.filter({$0.suit == .joker}).count
        let potentialSequences = currentHand
            .filter({$0.suit != .joker})
            .groupBy(characteristic: Suit.self, sortedBy: Deck.yanivSort)
            .filter({$0.count >= 3 - numberOfJokersInHand})
        
        if potentialSequences.count > 0 {
            var bestSequence: [Card] = []
            for sequence in potentialSequences {
                let biggestGapInSequence = Deck.largestSequence(inCards: sequence, withGapLessThan: numberOfJokersInHand)
                if biggestGapInSequence.count > bestSequence.count {
                    bestSequence = biggestGapInSequence
                }
                
                // sequences with bigger
                if biggestGapInSequence.count == bestSequence.count && biggestGapInSequence.yanivRawValue > bestSequence.yanivRawValue {
                    bestSequence = biggestGapInSequence
                }
            }
            if bestSequence.count >= 3 {
                return DiscardStrategyResult(discardCards: bestSequence)
            }
        }
        
        // If no sequences found, return nothing to discard.
        return DiscardStrategyResult(discardCards: [])
    }
}
