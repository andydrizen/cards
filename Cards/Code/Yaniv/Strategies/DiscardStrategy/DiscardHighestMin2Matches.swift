import Foundation

struct DiscardHighestMin2Matches: DiscardStrategy {
    static var description: String = "DiscardHighestMin2Matches"
    
    static func discard(from currentHand: Deck) -> DiscardStrategyResult {
        let groupByFaces = currentHand
            .groupBy(characteristic: Face.self, sortedBy: Deck.yanivSort)
            .filter({$0.count >= 2})
        
        var cardsToDiscard = groupByFaces.reversed().first { group -> Bool in
            group.count == groupByFaces.first!.count
        }
        if cardsToDiscard == nil {
            cardsToDiscard = []
        }
        return DiscardStrategyResult(discardCards: cardsToDiscard!)
    }
}
