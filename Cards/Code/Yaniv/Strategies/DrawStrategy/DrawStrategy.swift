import Foundation

protocol DrawStrategy {
    static var description: String {get}
    static func draw(currentHand: Deck, availableDiscardedCards: [Card]) -> DrawStrategyResult
}

struct DrawStrategyResult {
    let drawCard: Card?
    let protectCards: [Card]
}
