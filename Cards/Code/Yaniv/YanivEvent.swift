import Foundation

public typealias PlayerIndex = Int
public enum YanivEvent: CustomStringConvertible {
    case updateYanivThreshold(Int)
    case startGame(Int)
    case startRound
    case startTurn(String)
    case drawPileReset
    case discard(String, [Card])
    case draw(String, Card?)
    case disqualify(String)
    case endGame
    case playerLost(String)
    case yaniv(String)
    case burn(String, String)
    case gameAbandoned(Int)
    
    public var description: String {
        switch self {
        case .updateYanivThreshold(let threshold):
            return "🎤 Yaniv threshold set to \(threshold)"
        case .startGame(let numberOfPlayers):
            return "🎤 Starting new game with \(numberOfPlayers) players"
        case .startRound:
            return "🎤 Starting new round"
        case .startTurn(let playerName):
            return "🎤 Starting \(playerName)'s turn"
        case .discard(let playerName, let cards):
            return "🎤 \(playerName) discarded \(cards)"
        case .drawPileReset:
            return "🎤 The draw pile has been restocked by shuffling the discard pile"
        case .draw(let playerName, let card):
            if let c = card {
                return "🎤 \(playerName) picked up \(c) from the discard pile"
            } else {
                return "🎤 \(playerName) picked up from the draw pile"
            }
        case .endGame:
            return "🎤 Game over"
        case .disqualify(let playerName):
            return "🎤 \(playerName) has been disqualified"
        case .playerLost(let playerName):
            return "🎤 \(playerName) has lost"
        case .yaniv(let playerName):
            return "\(playerName) called 🎉 YANIV!"
        case .burn(let yanivPlayerName, let burnPlayerName):
            return "\(yanivPlayerName) 🔥🔥🔥 burned by \(burnPlayerName)!"
        case .gameAbandoned(let turns):
            return "☠️ Game abandoned by players after \(turns) turns ☠️"
        }
    }
}
