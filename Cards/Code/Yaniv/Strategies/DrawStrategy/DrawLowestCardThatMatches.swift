import Foundation

struct DrawLowestCardThatMatches: DrawStrategy {
    static var description: String = "DrawLowestCardThatMatches"
    
    static func draw(currentHand: Deck, availableDiscardedCards: [Card]) -> DrawStrategyResult {
        // Is there an available card with the same face as something we've already got?
        var drawCard: Card?
        var protectedCards = [Card]()

        for discardedCard in availableDiscardedCards.sorted(by: Deck.yanivSort) {
            let matchingFaceCardsInHand = currentHand.filter({$0.face == discardedCard.face})
            if matchingFaceCardsInHand.count > 0 {
                drawCard = discardedCard
                protectedCards = matchingFaceCardsInHand
            }
        }
        
        return DrawStrategyResult(drawCard: drawCard, protectCards:protectedCards)
    }
}
