import XCTest
@testable import Cards

class YanivTests: XCTestCase {
    
    let sequenceHand = [
        Card(face: .ace, suit: .club),
        Card(face: .two, suit: .club),
        Card(face: .three, suit: .club),
        Card(face: .four, suit: .club),
        Card(face: .five, suit: .club)
    ]
    
    let jokerSequenceHand = [
        Card(face: .joker, suit: .joker),
        Card(face: .joker, suit: .joker),
        Card(face: .three, suit: .club),
        Card(face: .four, suit: .club),
        Card(face: .five, suit: .club)
    ]
    
    let matchingHand = [
        Card(face: .ace, suit: .diamond),
        Card(face: .ace, suit: .club),
        Card(face: .three, suit: .club),
        Card(face: .joker, suit: .joker),
        Card(face: .joker, suit: .joker)
    ]
    
    func testValidityDiscardingSequenceWithJokerCard() {
        let hand = [
            Card(face: .jack, suit: .spade),
            Card(face: .nine, suit: .spade),
            Card(face: .joker, suit: .joker),
            Card(face: .three, suit: .heart),
            Card(face: .nine, suit: .diamond)
        ]
        
        let discard = [
            Card(face: .nine, suit: .spade),
            Card(face: .joker, suit: .joker),
            Card(face: .jack, suit: .spade)
        ]
        
        let lastDiscard = [
            Card(face: .queen, suit: .spade)
        ]
        
        XCTAssertTrue(Yaniv.isValid(discard: discard, fromHand: hand, lastDiscardedCards: lastDiscard))
    }
    
    func testValidityDiscardingOneOwnedCard() {
        let hand = sequenceHand
        
        let discard = [
            Card(face: .ace, suit: .club)
        ]
        
        let lastDiscard = [
            Card(face: .ace, suit: .heart)
        ]
        
        XCTAssertTrue(Yaniv.isValid(discard: discard, fromHand: hand, lastDiscardedCards: lastDiscard))
    }
    
    func testValidityDiscardingUnownedCard() {
        let hand = sequenceHand
        
        let discard = [
            Card(face: .eight, suit: .heart)
        ]
        
        let lastDiscard = [
            Card(face: .ace, suit: .heart)
        ]
        
        XCTAssertFalse(Yaniv.isValid(discard: discard, fromHand: hand, lastDiscardedCards: lastDiscard))
    }
    
    func testValidityDiscardingOwnedSequenceCard() {
        let hand = sequenceHand
        
        let discard = [
            Card(face: .three, suit: .club),
            Card(face: .four, suit: .club),
            Card(face: .five, suit: .club)
        ]
        
        let lastDiscard = [
            Card(face: .ace, suit: .heart)
        ]
        
        XCTAssertTrue(Yaniv.isValid(discard: discard, fromHand: hand, lastDiscardedCards: lastDiscard))
    }
    
    func testValidityDiscardingOwnedSequenceWithJokersCard() {
        let hand = jokerSequenceHand
        
        let discard = [
            Card(face: .three, suit: .club),
            Card(face: .joker, suit: .joker),
            Card(face: .five, suit: .club)
        ]
        
        let lastDiscard = [
            Card(face: .ace, suit: .heart)
        ]
        
        XCTAssertTrue(Yaniv.isValid(discard: discard, fromHand: hand, lastDiscardedCards: lastDiscard))
    }
    
    func testValidityDiscardingOwnedNonSequenceWithJokersCard() {
        let hand = jokerSequenceHand
        
        let discard = [
            Card(face: .joker, suit: .joker),
            Card(face: .three, suit: .club),
            Card(face: .five, suit: .club)
        ]
        
        let lastDiscard = [
            Card(face: .ace, suit: .heart)
        ]
        
        XCTAssertFalse(Yaniv.isValid(discard: discard, fromHand: hand, lastDiscardedCards: lastDiscard))
    }
    
    func testValidityDiscardingOwnedSequenceWithJokersOutsideCard() {
        let hand = jokerSequenceHand
        
        let discard = [
            Card(face: .joker, suit: .joker),
            Card(face: .three, suit: .club),
            Card(face: .four, suit: .club),
            Card(face: .five, suit: .club),
            Card(face: .joker, suit: .joker)
        ]
        
        let lastDiscard = [
            Card(face: .ace, suit: .heart)
        ]
        
        XCTAssertTrue(Yaniv.isValid(discard: discard, fromHand: hand, lastDiscardedCards: lastDiscard))
    }
    
    func testValidityDiscardingUnownedSequenceCard() {
        let hand = sequenceHand
        
        let discard = [
            Card(face: .three, suit: .spade),
            Card(face: .four, suit: .spade),
            Card(face: .five, suit: .spade)
        ]
        
        let lastDiscard = [
            Card(face: .ace, suit: .heart)
        ]
        
        XCTAssertFalse(Yaniv.isValid(discard: discard, fromHand: hand, lastDiscardedCards: lastDiscard))
    }
    
    func testValidityDiscardingTwoMatchingOwnedCard() {
        let hand = matchingHand
        
        let discard = [
            Card(face: .ace, suit: .club),
            Card(face: .ace, suit: .diamond)
        ]
        
        let lastDiscard = [
            Card(face: .ace, suit: .heart)
        ]
        
        XCTAssertTrue(Yaniv.isValid(discard: discard, fromHand: hand, lastDiscardedCards: lastDiscard))
    }
    
    func testValidityDiscardingTwoMatchingAndJokerOwnedCard() {
        let hand = matchingHand
        
        let discard = [
            Card(face: .ace, suit: .club),
            Card(face: .ace, suit: .diamond),
            Card(face: .joker, suit: .joker)
        ]
        
        let lastDiscard = [
            Card(face: .ace, suit: .heart)
        ]
        
        XCTAssertTrue(Yaniv.isValid(discard: discard, fromHand: hand, lastDiscardedCards: lastDiscard))
    }
    
    func testValidityDiscardingTwoJokersOwnedCard() {
        let hand = matchingHand
        
        let discard = [
            Card(face: .joker, suit: .joker),
            Card(face: .joker, suit: .joker)
        ]
        
        let lastDiscard = [
            Card(face: .ace, suit: .heart)
        ]
        
        XCTAssertTrue(Yaniv.isValid(discard: discard, fromHand: hand, lastDiscardedCards: lastDiscard))
    }
    
    func testGroupByFace() {
        let hand = [
            Card(face: .king, suit: .heart),
            Card(face: .ace, suit: .club),
            Card(face: .three, suit: .club),
            Card(face: .ace, suit: .diamond),
            Card(face: .king, suit: .spade)
        ]
        
        let actualGroups = hand.groupBy(characteristic: Face.self, sortedBy: Deck.yanivSort)
        let expectedGroup = [
            [
                Card(face: .ace, suit: .club),
                Card(face: .ace, suit: .diamond)
            ],
            [
                Card(face: .king, suit: .heart),
                Card(face: .king, suit: .spade)
            ],
            [
                Card(face: .three, suit: .club)
            ]
        ]
        
        XCTAssertEqual(actualGroups.count, expectedGroup.count)
        for i in 0..<expectedGroup.count {
            XCTAssertEqual(actualGroups[i], expectedGroup[i])
        }
    }
    
    func testGroupBySuit() {
        let hand = [
            Card(face: .king, suit: .heart),
            Card(face: .king, suit: .club),
            Card(face: .three, suit: .club),
            Card(face: .ace, suit: .diamond),
            Card(face: .king, suit: .spade)
        ]
        
        let actualGroups = hand.groupBy(characteristic: Suit.self, sortedBy: Deck.yanivSort)
        let expectedGroup = [
            [
                Card(face: .three, suit: .club),
                Card(face: .king, suit: .club)
            ],
            [
                Card(face: .ace, suit: .diamond)
            ],
            [
                Card(face: .king, suit: .heart)
            ],
            [
                Card(face: .king, suit: .spade)
            ]
        ]
        
        XCTAssertEqual(actualGroups.count, expectedGroup.count)
        for i in 0..<expectedGroup.count {
            XCTAssertEqual(actualGroups[i], expectedGroup[i])
        }
    }
    
    func testSorted() {
        let hand = [
            Card(face: .three, suit: .club),
            Card(face: .ace, suit: .diamond),
            Card(face: .king, suit: .spade),
            Card(face: .king, suit: .heart),
            Card(face: .ace, suit: .club)
        ]
        
        let expectedHand = [
            Card(face: .ace, suit: .club),
            Card(face: .ace, suit: .diamond),
            Card(face: .three, suit: .club),
            Card(face: .king, suit: .heart),
            Card(face: .king, suit: .spade)
        ]
        
        let sortedHand = hand.sorted(by: Deck.yanivSort)
        
        XCTAssertEqual(expectedHand, sortedHand)
    }
    
    func testLargestSequence() {
        let hand = [
            Card(face: .three, suit: .club),
            Card(face: .four, suit: .club),
            Card(face: .six, suit: .club),
            Card(face: .king, suit: .club)
        ]
        
        let expectedLargestSequence = [
            Card(face: .three, suit: .club),
            Card(face: .four, suit: .club)
        ]
        
        let actualLargestSequence = Deck.largestSequence(inCards:hand, withGapLessThan: 0)
        
        XCTAssertEqual(expectedLargestSequence, actualLargestSequence)
    }
    
    func testNumberOfGaps() {
        let hand = [
            Card(face: .three, suit: .club),
            Card(face: .four, suit: .club),
            Card(face: .six, suit: .club),
            Card(face: .eight, suit: .club)
        ]
        
        let actualGaps = Deck.numberOfGapsIn(sequence: hand)
        
        XCTAssertEqual(actualGaps, 2)
    }
    
    func testLargestSequenceWith1Gap() {
        let hand = [
            Card(face: .three, suit: .club),
            Card(face: .four, suit: .club),
            Card(face: .six, suit: .club),
            Card(face: .eight, suit: .club)
        ]
        
        let expectedLargestSequence = [
            Card(face: .three, suit: .club),
            Card(face: .four, suit: .club),
            Card(face: .joker, suit: .joker),
            Card(face: .six, suit: .club)
        ]
        
        let actualLargestSequence = Deck.largestSequence(inCards:hand, withGapLessThan: 1)
        
        XCTAssertEqual(expectedLargestSequence, actualLargestSequence)
    }
    
    func testLargestSequenceWith2Gaps() {
        let hand = [
            Card(face: .ace, suit: .club),
            Card(face: .three, suit: .club),
            Card(face: .five, suit: .club),
            Card(face: .eight, suit: .club),
            Card(face: .jack, suit: .heart),
        ]
        
        let potentialSequences = hand.groupBy(characteristic: Suit.self, sortedBy: Deck.yanivSort).first!
        
        let expectedLargestSequence = [
            Card(face: .ace, suit: .club),
            Card(face: .joker, suit: .joker),
            Card(face: .three, suit: .club),
            Card(face: .joker, suit: .joker),
            Card(face: .five, suit: .club),
        ]
        
        let actualLargestSequence = Deck.largestSequence(inCards:potentialSequences, withGapLessThan: 2)
        
        XCTAssertEqual(expectedLargestSequence, actualLargestSequence)
    }
    
    func testLargestSequenceWith2Jokers() {
        let hand = [
            Card(face: .seven, suit: .club),
            Card(face: .four, suit: .club),
            Card(face: .joker, suit: .joker),
            Card(face: .joker, suit: .joker),
            Card(face: .queen, suit: .diamond),
            ]
        
        let potentialSequences = hand
            .filter({$0.suit != .joker})
            .groupBy(characteristic: Suit.self, sortedBy: Deck.yanivSort).first!
        
        let expectedLargestSequence = [
            Card(face: .four, suit: .club),
            Card(face: .joker, suit: .joker),
            Card(face: .joker, suit: .joker),
            Card(face: .seven, suit: .club),
            ]
        
        let actualLargestSequence = Deck.largestSequence(inCards:potentialSequences, withGapLessThan: 2)
        
        XCTAssertEqual(expectedLargestSequence, actualLargestSequence)
    }
    
    func testNoSequence() {
        
        let hand = [
            Card(face: .joker, suit: .joker),
            Card(face: .eight, suit: .heart),
            Card(face: .nine, suit: .club),
            Card(face: .queen, suit: .spade),
            Card(face: .five, suit: .club)
        ]
        let numberOfJokersInHand = 1
        
        let actualLargestSequence = Deck.largestSequence(inCards: hand, withGapLessThan: numberOfJokersInHand)
        
        XCTAssertEqual(actualLargestSequence.count, 0)
    }
    
    func testDiscardSequence() {
        let hand = [
            Card(face: .joker, suit: .joker),
            Card(face: .eight, suit: .heart),
            Card(face: .seven, suit: .club),
            Card(face: .queen, suit: .spade),
            Card(face: .five, suit: .club)
        ]
        
        let expectedDiscard = [
            Card(face: .five, suit: .club),
            Card(face: .joker, suit: .joker),
            Card(face: .seven, suit: .club)
        ]
        
        let actualDiscard = DiscardMin3Sequence.discard(from: hand).discardCards
        
        XCTAssertEqual(expectedDiscard, actualDiscard)
    }
    
    func testDiscardMin4Sequence() {
        let hand = [
            Card(face: .joker, suit: .joker),
            Card(face: .eight, suit: .heart),
            Card(face: .seven, suit: .club),
            Card(face: .queen, suit: .spade),
            Card(face: .five, suit: .club)
        ]
        
        let expectedDiscard = [Card]()
        
        let actualDiscard = DiscardMin4Sequence.discard(from: hand).discardCards
        
        XCTAssertEqual(expectedDiscard, actualDiscard)
    }
    
    func testDiscardHighestMin3Matches() {
        let hand = [
            Card(face: .ace, suit: .diamond),
            Card(face: .ace, suit: .club),
            Card(face: .eight, suit: .diamond),
            Card(face: .eight, suit: .heart),
            Card(face: .eight, suit: .club)
        ]
        
        let expectedDiscard = [
            Card(face: .eight, suit: .club),
            Card(face: .eight, suit: .diamond),
            Card(face: .eight, suit: .heart)
        ]
        
        let actualDiscard = DiscardHighestMin3Matches.discard(from: hand).discardCards
        
        XCTAssertEqual(expectedDiscard, actualDiscard)
    }
    
    func testDiscardLowestMin2Matches() {
        let hand = [
            Card(face: .ace, suit: .diamond),
            Card(face: .ace, suit: .club),
            Card(face: .eight, suit: .diamond),
            Card(face: .eight, suit: .heart),
            Card(face: .eight, suit: .club)
        ]
        
        let expectedDiscard = [
            Card(face: .ace, suit: .club),
            Card(face: .ace, suit: .diamond)
        ]
        
        let actualDiscard = DiscardLowestMin2Matches.discard(from: hand).discardCards
        
        XCTAssertEqual(expectedDiscard, actualDiscard)
    }
    
    func testDiscardHighest() {
        let hand = [
            Card(face: .ace, suit: .diamond),
            Card(face: .ace, suit: .club),
            Card(face: .eight, suit: .diamond),
            Card(face: .eight, suit: .heart),
            Card(face: .eight, suit: .club)
        ]
        
        let expectedDiscard = [
            Card(face: .eight, suit: .heart)
        ]
        
        let actualDiscard = DiscardHighest.discard(from: hand).discardCards
        
        XCTAssertEqual(expectedDiscard, actualDiscard)
    }
    
    func testDiscardLowest() {
        let hand = [
            Card(face: .ace, suit: .diamond),
            Card(face: .ace, suit: .club),
            Card(face: .eight, suit: .diamond),
            Card(face: .eight, suit: .heart),
            Card(face: .eight, suit: .club)
        ]
        
        let expectedDiscard = [
            Card(face: .ace, suit: .club)
        ]
        
        let actualDiscard = DiscardLowest.discard(from: hand).discardCards
        
        XCTAssertEqual(expectedDiscard, actualDiscard)
    }
    
    func testDrawJoker() {
        let hand = [
            Card(face: .ace, suit: .diamond),
            Card(face: .ace, suit: .club),
            Card(face: .eight, suit: .diamond),
            Card(face: .eight, suit: .heart),
            Card(face: .eight, suit: .club)
        ]
        
        let available = [
            Card(face: .ace, suit: .heart),
            Card(face: .joker, suit: .joker)
        ]
        
        let expectedDraw: Card? = Card(face: .joker, suit: .joker)
        let actualDraw = DrawJoker.draw(currentHand: hand, availableDiscardedCards: available).drawCard
        
        XCTAssertEqual(expectedDraw, actualDraw)
    }
    
    func testDrawHighestCardThatMatches() {
        let hand = [
            Card(face: .ace, suit: .diamond),
            Card(face: .ace, suit: .club),
            Card(face: .eight, suit: .diamond),
            Card(face: .eight, suit: .heart),
            Card(face: .eight, suit: .club)
        ]
        
        let available = [
            Card(face: .eight, suit: .spade),
            Card(face: .king, suit: .spade)
        ]
        
        let expectedDraw: Card? = Card(face: .eight, suit: .spade)
        let actualDraw = DrawHighestCardThatMatches.draw(currentHand: hand, availableDiscardedCards: available).drawCard
        
        XCTAssertEqual(expectedDraw, actualDraw)
    }
    
    func testDrawLowestCardThatMatches() {
        let hand = [
            Card(face: .ace, suit: .diamond),
            Card(face: .ace, suit: .club),
            Card(face: .eight, suit: .diamond),
            Card(face: .eight, suit: .heart),
            Card(face: .joker, suit: .joker)
        ]
        
        let available = [
            Card(face: .ace, suit: .spade),
            Card(face: .king, suit: .spade)
        ]
        
        let expectedDraw: Card? = Card(face: .ace, suit: .spade)
        let actualDraw = DrawLowestCardThatMatches.draw(currentHand: hand, availableDiscardedCards: available).drawCard
        
        XCTAssertEqual(expectedDraw, actualDraw)
    }
    
    func testDrawLowestCardThatSequences() {
        let hand = [
            Card(face: .ace, suit: .diamond),
            Card(face: .two, suit: .diamond),
            Card(face: .eight, suit: .diamond),
            Card(face: .ten, suit: .diamond),
            Card(face: .joker, suit: .joker)
        ]
        
        let available = [
            Card(face: .three, suit: .diamond),
            Card(face: .nine, suit: .diamond)
        ]
        
        let expectedDraw: Card? = Card(face: .three, suit: .diamond)
        let actualDraw = DrawLowestCardThatSequences.draw(currentHand: hand, availableDiscardedCards: available).drawCard
        
        XCTAssertEqual(expectedDraw, actualDraw)
    }
    
    func testDrawHighestCardThatSequences() {
        let hand = [
            Card(face: .ace, suit: .diamond),
            Card(face: .two, suit: .diamond),
            Card(face: .eight, suit: .diamond),
            Card(face: .ten, suit: .diamond),
            Card(face: .joker, suit: .joker)
        ]
        
        let available = [
            Card(face: .three, suit: .diamond),
            Card(face: .nine, suit: .diamond)
        ]
        
        let expectedDraw: Card? = Card(face: .nine, suit: .diamond)
        let actualDraw = DrawHighestCardThatSequences.draw(currentHand: hand, availableDiscardedCards: available).drawCard
        
        XCTAssertEqual(expectedDraw, actualDraw)
    }
    
}
