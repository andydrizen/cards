import Foundation

public typealias Deck = [Card]
public extension Array where Element == Card {
    public static func standard(includeJokers: Bool) -> Deck {
        var deck = Deck()
        for suit in [Suit.spade, Suit.heart, Suit.diamond, Suit.club] {
            let newCards = [
                Card(face: .ace, suit: suit),
                Card(face: .two, suit: suit),
                Card(face: .three, suit: suit),
                Card(face: .four, suit: suit),
                Card(face: .five, suit: suit),
                Card(face: .six, suit: suit),
                Card(face: .seven, suit: suit),
                Card(face: .eight, suit: suit),
                Card(face: .nine, suit: suit),
                Card(face: .ten, suit: suit),
                Card(face: .jack, suit: suit),
                Card(face: .queen, suit: suit),
                Card(face: .king, suit: suit)
            ]
            deck.append(contentsOf: newCards)
        }
        if includeJokers {
            2.times {
                deck.append(Card(face: .joker, suit: .joker))
            }
        }
        
        return deck
    }
    
    @discardableResult
    public mutating func move(numberOfCards: Int, to: inout Deck) -> [Card] {
        let initialFromCount = count
        let initialToCount = to.count
        
        let cardsToBeMoved = self.removing(numberOfCards: numberOfCards)
        to.adding(cards: cardsToBeMoved)
        
        if initialFromCount - cardsToBeMoved.count != count {
            preconditionFailure()
        }
        
        if initialToCount + cardsToBeMoved.count != to.count {
            preconditionFailure()
        }
        return cardsToBeMoved
    }
    
    @discardableResult
    public mutating func move(cards: [Card], to: inout Deck) -> [Card] {
        let initialFromCount = count
        let initialToCount = to.count
        
        let cardsToBeMoved = self.removing(cards: cards)
        to.adding(cards: cardsToBeMoved)
        
        if initialFromCount - cardsToBeMoved.count != count {
            preconditionFailure()
        }
        
        if initialToCount + cardsToBeMoved.count != to.count {
            preconditionFailure()
        }
        return cardsToBeMoved
    }
    
    public mutating func deal(numberOfCards: Int, to: inout [Deck]) {
        numberOfCards.times {
            for i in 0..<to.count {
                _ = move(numberOfCards: 1, to: &to[i])
            }
        }
    }
    
    mutating func adding(cards: [Card]) {
        append(contentsOf: cards)
    }
    
    @discardableResult
    mutating func removing(numberOfCards: Int) -> [Card] {
        var takenCards = [Card]()
        
        for _ in 0..<numberOfCards {
            if count > 0 {
                takenCards.append(removeFirst())
            }
        }
        
        return takenCards
    }
    
    @discardableResult
    mutating func removing(cards: [Card]) -> [Card] {
        var removedCards = [Card]()
        for card in cards {
            let i = index(where: { innerCard -> Bool in
                return card == innerCard
            })
            
            if let ind = i {
                removedCards.append(remove(at: ind))
            }
        }
        return removedCards
    }
    
    public func groupBy<T>(characteristic: T.Type, sortedBy: (Card, Card) -> Bool) -> [[Card]] where T: CardCharacteristic {
        var group = [T: [Card]]()
        
        for c in self {
            let index: T
            if characteristic == Face.self {
                index = c.face as! T
            } else if characteristic == Suit.self {
                index = c.suit as! T
            } else {
                preconditionFailure("WTF")
            }
            
            if group[index] == nil {
                group[index] = [Card]()
            }
            group[index]?.append(c)
        }
        
        return group.map({ (item) -> [Card] in
            item.value.sorted(by: sortedBy)
        }).sorted(by: { (a, b) -> Bool in
            if a.count == b.count {
                return sortedBy(a.first!, b.first!)
            }
            
            return a.count > b.count
        })
    }
}
