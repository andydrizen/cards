import Foundation

protocol DiscardStrategy {
    static var description: String {get}
    static func discard(from currentHand: Deck) -> DiscardStrategyResult
}

struct DiscardStrategyResult {
    let discardCards: [Card]
}
