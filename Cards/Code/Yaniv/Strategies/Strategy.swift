import Foundation

public class Strategy: YanivStrategy {
    public var yanivThreshold: Int = Int.max
    
    var cardKnowledge = [String : Deck]()
    var currentPlayerName: String?
    
    let discardPreferences: [DiscardStrategy.Type]
    let drawPreferences: [DrawStrategy.Type]
    
    public var description: String {
        return "\t---: \(discardPreferences)\n\t+++: \(drawPreferences)"
    }
    
    init(discardPreferences: [DiscardStrategy.Type], drawPreferences: [DrawStrategy.Type]) {
        self.discardPreferences = discardPreferences
        self.drawPreferences = drawPreferences
    }
    
    public func handleEvent(event: YanivEvent) {
        switch event {
        case .updateYanivThreshold(let newThreshold):
            yanivThreshold = newThreshold
        case .startGame:
            break
        case .startRound:
            cardKnowledge = [String : Deck]()
            cardKnowledge["draw"] = Deck.standard(includeJokers: true)
            cardKnowledge["discard"] = Deck()
            break
        case .endGame:
            break
        case .discard(let playerName, let cards):
            if cardKnowledge[playerName] == nil {
                cardKnowledge[playerName] = []
            }
            
            cardKnowledge["draw"]?.removing(cards: cards)
            cardKnowledge[playerName]?.removing(cards: cards)
            cardKnowledge["discard"]?.adding(cards: cards)
            break
        case .draw(let playerName, let card):
            if cardKnowledge[playerName] == nil {
                cardKnowledge[playerName] = []
            }
            
            // the player picked up from the discard pile
            if let card = card {
                cardKnowledge[playerName]?.adding(cards: [card])
                cardKnowledge["discard"]?.removing(cards: [card])
            }
            break
        case .drawPileReset:
            cardKnowledge["draw"] = Deck.standard(includeJokers: true)
            cardKnowledge["discard"] = []
            break
        case .disqualify(_):
            break
        case .startTurn(let playerName):
            currentPlayerName = playerName
            break
        case .playerLost(_):
            break
        case .yaniv(_):
            break
        case .burn(_, _):
            break
        case .gameAbandoned(_):
            break
        }
    }
    
    struct MoveOption {
        let draw: Card?
        let discard: [Card]
        let numberOfCardsPostMove: Int
        let handValuePostMove: Int
    }
    
    public func move(currentHand: Deck, availableDiscardedCards: [Card], playerIndex: Int) -> YanivTurn {
        cardKnowledge["draw"]?.removing(cards: currentHand)
        if let playerName = currentPlayerName {
            cardKnowledge[playerName] = currentHand
        }
        
        if currentHand.yanivFaceValue <= yanivThreshold {
            return .yaniv
        }
        
        var options = [MoveOption]()
        
        for drawStrategy in drawPreferences {
            var drawResult = drawStrategy.draw(currentHand: currentHand, availableDiscardedCards: availableDiscardedCards)
            
            var discardableCards = currentHand
            discardableCards.removing(cards: drawResult.protectCards)
            
            if discardableCards.isEmpty {
                continue
            }
            
            for discardStrategy in discardPreferences {
                let discardResult = discardStrategy.discard(from: discardableCards)
                
                if discardResult.discardCards.isEmpty {
                    continue
                }
                
                var newHand = currentHand
                newHand.removing(cards: discardResult.discardCards)
                
                if let newCard = drawResult.drawCard {
                    newHand.adding(cards: [newCard])
                } else {
                    cardKnowledge["draw"]?.removing(cards: availableDiscardedCards)
                    
                    // 5% chance we go to the draw pile to prevent infinite loops whereby
                    // two players just swap cards forever.
                    let random = arc4random_uniform(100)
                    if let dp = cardKnowledge["draw"], random < 5 {
                        let expectedNextDrawPileCard = Int(round(Double(dp.yanivFaceValue)/Double(dp.count)))
                        let lowestDiscardedCard = availableDiscardedCards.sorted(by: Deck.yanivSort).first!
                        if expectedNextDrawPileCard > [lowestDiscardedCard].yanivFaceValue {
                            newHand.adding(cards: [lowestDiscardedCard])
                            drawResult = DrawStrategyResult(drawCard: lowestDiscardedCard, protectCards: drawResult.protectCards)
                        }
                    }
                }
                
                let option = MoveOption(draw: drawResult.drawCard,
                                        discard: discardResult.discardCards,
                                        numberOfCardsPostMove: currentHand.count + 1 - discardResult.discardCards.count,
                                        handValuePostMove: newHand.yanivFaceValue)
                options.append(option)
            }
        }
        
        let sortedOptions = options.sorted { (moveA, moveB) -> Bool in
            moveA.handValuePostMove < moveB.handValuePostMove
        }
        
        if let chosenOption = sortedOptions.first {
            return YanivTurn.discardAndDraw(chosenOption.discard, chosenOption.draw)
        } else {
            return YanivTurn.discardAndDraw([currentHand.first!], nil)
        }
    }
}
