import Foundation

struct DrawHighestCardThatSequences: DrawStrategy {
    static var description: String = "DrawHighestCardThatSequences"
    
    static func draw(currentHand: Deck, availableDiscardedCards: [Card]) -> DrawStrategyResult {
        var drawCard: Card?
        var protectedCards = [Card]()
        for discardedCard in availableDiscardedCards.sorted(by: Deck.yanivSort) {
            
            var newHand = currentHand
            newHand.adding(cards: [discardedCard])
            let numberOfJokersInHand = newHand.filter({$0 == Card(face: .joker, suit: .joker)}).count
            let sequence = Deck.largestSequence(inCards: newHand, withGapLessThan: numberOfJokersInHand)
            if sequence.count >= 2 {
                drawCard = discardedCard
                protectedCards = newHand.removing(cards: [discardedCard])
            }
            
        }
        return DrawStrategyResult(drawCard: drawCard, protectCards:protectedCards)
    }
}
