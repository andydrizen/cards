import Foundation

public final class Yaniv {
    private var yanivThreshold: Int
    private var losingScore: Int
    private var turnIndex: Int
    private var players: [YanivPlayer]
    
    private var drawPile: Deck
    private var discardPile: Deck
    private var lastDiscardedCards: Deck
    
    var debugTracking = false
    var turnLimit = 2000
    
    public init(players: [YanivPlayer], yanivThreshold: Int = 7, losingScore: Int = 200) {
        
        // Decide on the rules
        self.yanivThreshold = yanivThreshold
        self.losingScore = losingScore
        
        // Seat the players and choose who goes first
        self.players = players
        turnIndex = 0
        
        // Initialise with temporary values, just so that I can move the real initialisation to a reusable func.
        drawPile = Deck()
        discardPile = Deck()
        lastDiscardedCards = Deck()
        
        track(event: .updateYanivThreshold(yanivThreshold))
    }
    
    public func evaluate() {
        var totalTurns = 0
        track(event: .startGame(players.count))
        prepareRound()
        
        // Whilst there are still two players in the game
        while players.count >= 2 {
            if totalTurns > turnLimit {
                track(event: .gameAbandoned(totalTurns))
                break
            }
            
            totalTurns += 1
            turn()
        }
        
        track(event: .endGame)
    }
    
    private func turn() {
        let playerIndex = turnIndex % players.count
        let player = players[playerIndex]
        
        if player.score > 200 {
            turnIndex += 1
            return
        }
        
        track(event: .startTurn(player.name))
        
        if drawPile.count == 0 {
            _ = discardPile.move(numberOfCards: discardPile.count, to: &drawPile)
            drawPile.shuffle()
            drawPile.move(cards: lastDiscardedCards, to: &discardPile)
            track(event: .drawPileReset)
        }

        switch player.move(availableDiscardedCards: Yaniv.availableDiscardedCards(lastDiscarded: lastDiscardedCards), playerIndex: playerIndex) {
        case .yaniv:
            if player.hand.yanivFaceValue <= yanivThreshold {
                track(event: .yaniv(player.name))
                let winningPlayerIndex = determineWinningPlayerIndex()
                turnIndex = winningPlayerIndex
                adjustScoresDueToYanivCalledBy(playerIndex: playerIndex)
                prepareRound()
            } else {
                disqualify(player: player, atIndex: playerIndex)
            }
        case .discardAndDraw(let discardedCards, let drawCard):
            
            // Validate the the discarded card(s).
            if !Yaniv.isValid(discard: discardedCards, fromHand: player.hand, lastDiscardedCards: lastDiscardedCards) {
                disqualify(player: player, atIndex: playerIndex)
            }
            
            // If the player wants to pick from the discard pile, validate the choice.
            if drawCard != nil && !Yaniv.isValid(draw: drawCard!, lastDiscardedCards: lastDiscardedCards) {
                disqualify(player: player, atIndex: playerIndex)
            }
            
            // Perform discard
            lastDiscardedCards = player.hand.move(cards: discardedCards, to: &discardPile)
            
            // Perform draw
            if let cardFromDiscardPile = drawCard {
                discardPile.move(cards: [cardFromDiscardPile], to: &player.hand)
            } else {
                drawPile.move(numberOfCards: 1, to: &player.hand)
            }

            track(event: .discard(player.name, lastDiscardedCards))
            track(event: .draw(player.name, drawCard))
            turnIndex += 1
        }
    }
    
    func disqualify(player: YanivPlayer, atIndex playerIndex: PlayerIndex) {
        player.increaseScore(by: 99999)
        track(event: .disqualify(player.name))
    }
    
    func determineWinningPlayerIndex() -> Int {
        let minScore = players.map({$0.hand.yanivFaceValue}).min()
        let winningPlayer = players.index { player -> Bool in
            player.hand.yanivFaceValue == minScore!
        }
        
        return winningPlayer!
    }
    
    func adjustScoresDueToYanivCalledBy(playerIndex: Int) {
        let callingPlayersHandValue = players[playerIndex].hand.yanivFaceValue
        let assafPlayers = players.filter({$0 !== players[playerIndex]}).filter({$0.hand.yanivFaceValue <= callingPlayersHandValue})
        
        for i in 0..<players.count {
            let player = players[i]
            
            if i == playerIndex {
                let adjustment = assafPlayers.count * 30 + (assafPlayers.count > 0 ? callingPlayersHandValue : 0)
                player.increaseScore(by: adjustment)
                
                for j in 0..<assafPlayers.count {
                    track(event: .burn(player.name, assafPlayers[j].name))
                }
            } else {
                player.increaseScore(by: player.hand.yanivFaceValue)
            }
            
            if player.score == losingScore/2 {
                player.increaseScore(by: -losingScore/4)
            } else if player.score == losingScore {
                player.increaseScore(by: -losingScore/2)
            }
            
            if player.score > losingScore {
                track(event: .playerLost(player.name))
            }
        }
    }
    
    func prepareRound() {
        track(event: .startRound)
        
        // Shuffle the deck with jokers
        drawPile = Deck.standard(includeJokers: true).shuffled()
        discardPile = Deck()
        
        // Deal the initial hands
        
        players = players.filter({$0.score <= losingScore})
        
        var playerHands = [Deck](repeatElement(Deck(), count: players.count))
        drawPile.deal(numberOfCards: 5, to: &playerHands)
        for i in 0..<players.count {
            players[i].hand = playerHands[i]
        }
        
        // Turn over the first card
        lastDiscardedCards = drawPile.move(numberOfCards: 1, to: &discardPile)
    }
    
    func track(event: YanivEvent) {
        if debugTracking { print("\(event)") }
        
        for player in players {
            player.strategy.handleEvent(event: event)
        }
    }
    
    static func availableDiscardedCards(lastDiscarded: [Card]) -> [Card] {
        var nextPlayerChoices: [Card] = []
        if lastDiscarded.count == 1 {
            nextPlayerChoices = lastDiscarded
        } else {
            nextPlayerChoices = [lastDiscarded.first!, lastDiscarded.last!]
        }
        return nextPlayerChoices
    }
    
    static func isValid(draw card: Card, lastDiscardedCards: [Card]) -> Bool {
        let nextPlayerChoices = availableDiscardedCards(lastDiscarded: lastDiscardedCards)
        return nextPlayerChoices.contains(card)
    }
    
    static func isValid(discard: [Card], fromHand hand: [Card], lastDiscardedCards: [Card]) -> Bool {
        if discard.isEmpty {
            return false
        }
        
        // Check player holds these cards
        for card in discard {
            if !hand.contains(card) {
                return false
            }
        }
        
        let cardValues = discard.map({[$0].yanivRawValue})
        
        // Are the cards (excluding jokers) the same value?
        var valueSet = Set(cardValues)
        valueSet.remove(0)
        if valueSet.count > 1 {
            
            // They had better be a sequence then!
            
            // start from the first non-joker card
            let firstNonZeroValue = cardValues.filter({$0 != 0}).first!
            let offsetIndex = cardValues.index(of: firstNonZeroValue)!
            for i in 0..<cardValues.count-offsetIndex {
                if cardValues[i + offsetIndex] != firstNonZeroValue + i && cardValues[i + offsetIndex] != 0 {
                    return false
                }
            }
        }
        
        return true
    }
}

