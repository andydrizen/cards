import XCTest
@testable import Cards

class DeckTests: XCTestCase {
    
    func testMoveBetweenDecks() {
        
        var deck1 = Deck.standard(includeJokers: false)
        var deck2 = Deck()
        
        let card1 = Card(face: .ace, suit: .heart)
        let card2 = Card(face: .ace, suit: .club)
        
        deck1.move(cards: [
            card1,
            card2
        ], to: &deck2)
        
        XCTAssertTrue(deck2.contains(card1))
        XCTAssertTrue(deck2.contains(card2))
        
        XCTAssertFalse(deck1.contains(card1))
        XCTAssertFalse(deck1.contains(card2))
    }
    
    func testMoveBetweenDecksWhereCardIsNotInHand() {
        
        var deck1 = Deck()
        var deck2 = Deck()
        
        let card1 = Card(face: .ace, suit: .heart)
        let card2 = Card(face: .ace, suit: .club)
        
        deck1.move(cards: [
            card1,
            card2
            ], to: &deck2)
        
        XCTAssertFalse(deck1.contains(card1))
        XCTAssertFalse(deck1.contains(card2))
        XCTAssertFalse(deck2.contains(card1))
        XCTAssertFalse(deck2.contains(card2))
    }
}
