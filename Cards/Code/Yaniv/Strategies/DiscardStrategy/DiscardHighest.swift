import Foundation

struct DiscardHighest: DiscardStrategy {
    static var description: String = "DiscardHighest"
    
    static func discard(from currentHand: Deck) -> DiscardStrategyResult {
        let discard = currentHand.sorted(by: Deck.yanivSort).last!
        return DiscardStrategyResult(discardCards: [discard])
    }
}
