import Foundation

public enum YanivTurn {
    case discardAndDraw([Card], Card?)
    case yaniv
}

public protocol YanivStrategy: CustomStringConvertible{
    var yanivThreshold: Int { get }
    
    func handleEvent(event: YanivEvent)
    func move(currentHand: Deck, availableDiscardedCards: [Card], playerIndex: Int) -> YanivTurn
}
