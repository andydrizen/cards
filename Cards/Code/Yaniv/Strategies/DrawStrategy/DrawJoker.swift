import Foundation

struct DrawJoker: DrawStrategy {
    static var description: String = "DrawJoker"
    
    static func draw(currentHand: Deck, availableDiscardedCards: [Card]) -> DrawStrategyResult {
        var drawCard: Card?
        let joker = Card(face: .joker, suit: .joker)
        if availableDiscardedCards.contains(joker) {
            drawCard = joker
        }
        return DrawStrategyResult(drawCard: drawCard, protectCards: [])
    }
}
